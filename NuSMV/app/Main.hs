module Main where

import DotParser
import LTLNuSMV
import LTLParser
import Rers

import Control.Monad
import Data.List
import Data.Map ((!))
import Data.Maybe
import qualified Data.Set as Set
import System.Environment
import Text.Megaparsec

main :: IO ()
main = do
    -- Get 2 command line arguments
    [dotFile, ltlFile] <- getArgs

    -- Read the model, ;ine by line
    transitions <- mapMaybe (parseMaybe parseTransFull) . lines <$> readFile dotFile

    -- Get stuff
    let states  = Set.toList $ getStates transitions
    let inputs  = Set.toList $ getInputs transitions
    let outputs = ["oInv","oU","oV","oW","oX","oY","oZ"] -- fixed in rers2020
    let isBig = length inputs >= 15


    -- Start outputting NuSMV file
    putStrLn "MODULE main"


    -- Print all variables
    putStrLn "VAR"
    let printSet f = mconcat . intersperse ", " . map f
    putStrLn $ "    state  : {" ++ printSet id states  ++ "};"
    putStrLn $ "    input  : {" ++ printSet id inputs  ++ ", null};"
    putStrLn $ "    output : {" ++ printSet id outputs ++ ", null};"


    -- Initial state
    putStrLn "ASSIGN"
    let init = getInitial transitions
    putStrLn $ "    init(state)  := " ++  init  ++ ";"
    putStrLn $ "    init(output) := " ++ "null" ++ ";"


    -- Transitions
    let system = toMap isBig transitions
    let si = (,) <$> states <*> inputs
    let (_, deadsi) = partition (isValid system) si
    let printCond (s, i) = "state = " ++ s ++ " & input = " ++ i ++ " : "
    let printCase f psi = putStrLn $ "        " ++ printCond psi ++ f (system ! psi) ++ ";"

    -- Next states
    putStrLn "    next(state)  := case"
    forM_ si (printCase fst)
    putStrLn "        input = null : state;"
    putStrLn "    esac;"

    -- Next Outputs
    putStrLn "    next(output) := case"
    forM_ si (printCase snd)
    putStrLn "        input = null : null;"
    putStrLn "    esac;"


    -- Invariants: input and output alternate. And invalid inputs are invalid.
    let printInvar (s, i) = putStrLn $ "    & !(state = " ++ s ++ " & input = " ++ i ++ ")"
    putStrLn $ "INVAR !(input = null & output = null)"
    putStrLn $ "    & !(input != null & output != null)"
    forM_ deadsi printInvar


    -- Read the LTL properties, and rewrite to NuSMV
    properties <- mapMaybe (parseMaybe parseLTL) . lines <$> readFile ltlFile
    mapM_ (putStrLn . toNuSMV False) properties
