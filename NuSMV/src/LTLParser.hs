module LTLParser where

import LTL

import Control.Monad.Combinators.Expr
import Data.Void (Void)
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

import Prelude hiding (and, not, or, until)

{-
   This parses LTL formulas as given in the RERS challenge. It is not
   a fully fledged parser (for instance it does not do operator precedence
   well, but for RERS everything is bracketed anyways). All prefix operators
   (not, F, G, ...) have tighter binding.

   The atomic propositions are of type String.
-}

type Parser = Parsec Void String

type Proposition = String

sc :: Parser ()
sc = L.space space1 (L.skipLineComment "#") empty

lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc

symbol :: String -> Parser String
symbol = L.symbol sc

identifier :: Parser String
identifier = lexeme (some alphaNumChar)

parens :: Parser a -> Parser a
parens = between (symbol "(") (symbol ")")

parseLTL :: Parser (LTL Proposition)
parseLTL = makeExprParser parseTerm table where
    binaries = ["&" ==> AND, "|" ==> OR, "U" ==> U, "W" ==> W, "R" ==> R]
    prefixes = ["!" ==> NOT, "F" ==> F, "G" ==> G, "X" ==> X]
    table = [ [ pre t f | (t, f) <- prefixes ]
            , [ bin t f | (t, f) <- binaries ]
            ]
    bin name fun = InfixN (fun <$ symbol name)
    pre name fun = Prefix (fun <$ symbol name)
    (==>) = (,)

parseTerm :: Parser (LTL Proposition)
parseTerm = choice
  [ parens parseLTL
  , TRUE <$ symbol "true"
  , FALSE <$ symbol "false"
  , Literal <$> identifier
  ]
