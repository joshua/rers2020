module LTLNuSMV where

import LTL

import Data.Functor.Foldable

type Proposition = String

-- some basic bottom up rewriting to remove weak until and strong relase.
-- NuSMV only has until and release in the syntax (and past operators).
-- Also uses negated literals
removeWM :: LTL lit -> LTL lit
removeWM = cata alg where
    alg (NOTF (Literal x)) = NLiteral x
    alg (WF phi psi) = (phi `U` psi) `OR` G phi
    alg (MF phi psi) = (phi `R` psi) `AND` F phi
    alg x            = embed x

-- Simple pretty printer, using the kind of same syntax as the RERS challenge
toNuSMVString :: LTL Proposition -> String
toNuSMVString = cata alg where
    brackets s = "(" ++ s ++ ")"
    inOrOut 'i' = "input"
    inOrOut 'o' = "output"
    alg (LiteralF a)  = inOrOut (head a) ++ " = " ++ a
    alg (NLiteralF a) = inOrOut (head a) ++ " != " ++ a
    alg TRUEF         = "TRUE"
    alg FALSEF        = "FALSE"
    alg (ANDF s1 s2)  = brackets $ s1 ++ " & " ++ s2
    alg (ORF s1 s2)   = brackets $ s1 ++ " | " ++ s2
    alg (IMPLF s1 s2) = brackets $ s1 ++ " -> " ++ s2
    alg (NOTF s1)     = "! " ++ s1
    alg (FF s)        = "F " ++ s
    alg (GF s)        = "G " ++ s
    alg (XF s)        = "X " ++ s
    alg (UF s1 s2)    = brackets $ s1 ++ " U " ++ s2
    alg (RF s1 s2)    = brackets $ s1 ++ " V " ++ s2

-- Putting it together with an option to simplify first.
toNuSMV :: Bool -> LTL Proposition -> String
toNuSMV False = (++) "LTLSPEC " . toNuSMVString . removeWM
toNuSMV True  = (++) "LTLSPEC " . toNuSMVString . removeWM . simplify
