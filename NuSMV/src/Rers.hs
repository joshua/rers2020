module Rers where

import Data.Map (Map)
import qualified Data.Map as Map
import Data.Set (Set)
import qualified Data.Set as Set

type Stat = String
type Input = Int
type Output = Maybe Int
type Trans = (Stat, Stat, Input, Output)

-- Conversion for the output depends on type of problem
type IsBig = Bool

-- Convert the 1,2,3 (from the C file) to 'A','B','C' (from the ltl file) etc
toAlphI :: Input -> String
toAlphI i = 'i' : [['A'..'Z'] !! (i-1)]

-- Convert the 11,..,16 or 16,..,21 (from the C file) to 'U',..,'Z' (from the ltl file) etc
toAlphO :: IsBig -> Output -> String
toAlphO False (Just i) = 'o' : [['U'..'Z'] !! (i-11)]
toAlphO True (Just i) = 'o' : [['U'..'Z'] !! (i-16)]
toAlphO _ Nothing  = "oInv"


-- Set of states
getStates :: [Trans] -> Set Stat
getStates l = Set.fromList (froms ++ tos) where
    froms = map (\(a,_,_,_) -> a) l
    tos   = map (\(_,a,_,_) -> a) l

-- First transition in file is always from the initial state
getInitial :: [Trans] -> Stat
getInitial ((a,_,_,_):_) = a
getInitial _             = error "empty state space"

-- Input and output set
getInputs :: [Trans] -> Set String
getInputs = Set.fromList . map (\(_,_,i,_) -> toAlphI i)


-- Create a look up structure from the list of transitions. Also renames the
-- numbers into characters. Doesn't check for duplicates, we know Learnlib
-- generates a deterministic thing.
toMap :: IsBig -> [Trans] -> Map (Stat, String) (Stat, String)
toMap b trans = base where
    base = Map.fromList . map (\(from, to, i, o) -> ((from, toAlphI i), (to, toAlphO b o))) $ trans

isValid :: Map (Stat, String) (Stat, String) -> (Stat, String) -> Bool
isValid system p = case Map.lookup p system of
    Just (_, "oInv")  -> False
    Just (_, ('o':_)) -> True
    _                 -> False
