{-# language DeriveAnyClass #-}
{-# language DeriveFoldable #-}
{-# language DeriveFunctor #-}
{-# language DeriveGeneric #-}
{-# language DeriveTraversable #-}
{-# language PatternSynonyms #-}
{-# language TemplateHaskell #-}
{-# language TypeFamilies #-}

module LTL where

import Data.Functor.Foldable
import Data.Functor.Foldable.TH
import GHC.Generics (Generic)

import Prelude hiding (and, not, or, until)

{-
   A general LTL data type. It contains more than necessary for
   the RERS challenge. But it is nice to have flexibility. The
   NuSMV tool also allows more operators, so we might as well
   rewrite if we want.
-}
data LTL lit
    = Literal lit              -- Literal
    | NLiteral lit             -- Negated literal
    | TRUE                     -- True
    | FALSE                    -- False
    | AND (LTL lit) (LTL lit)  -- phi & psi
    | OR (LTL lit) (LTL lit)   -- phi | psi
    | IMPL (LTL lit) (LTL lit) -- phi -> psi
    | NOT (LTL lit)            -- ! phi
    | F (LTL lit)              -- Finally: F phi
    | G (LTL lit)              -- Globally: G phi
    | X (LTL lit)              -- Next: X phi
    | U (LTL lit) (LTL lit)    -- Until: phi U psi
    | W (LTL lit) (LTL lit)    -- Weak until: phi WU psi
    | R (LTL lit) (LTL lit)    -- Release: phi R psi
    | M (LTL lit) (LTL lit)    -- Strong relase: phi M psi
    deriving (Eq, Ord, Read, Show, Functor, Generic)

-- We derive a base functor, which allows us to use recusions schemes.
makeBaseFunctor ''LTL

-- some basic bottom up rewriting
-- most interesting are the true U x = F x and false R x = G x rules
simplify :: LTL lit -> LTL lit
simplify = cata alg where
    alg (ANDF TRUE s2)        = s2
    alg (ANDF FALSE _)        = FALSE
    alg (ANDF s1 TRUE)        = s1
    alg (ANDF _ FALSE)        = FALSE
    alg (ORF TRUE _)          = TRUE
    alg (ORF FALSE s2)        = s2
    alg (ORF _ TRUE)          = TRUE
    alg (ORF s1 FALSE)        = s1
    alg (ORF (NOT s1) s2)     = s1 `IMPL` s2
    alg (ORF (NLiteral l) s2) = Literal l `IMPL` s2
    alg (NOTF TRUE)           = FALSE
    alg (NOTF FALSE)          = TRUE
    alg (NOTF (Literal lit))  = NLiteral lit
    alg (NOTF (NLiteral lit)) = Literal lit
    alg (FF (F x))            = F x
    alg (FF TRUE)             = TRUE
    alg (FF FALSE)            = FALSE
    alg (GF (G x))            = G x
    alg (GF TRUE)             = TRUE
    alg (GF FALSE)            = FALSE
    alg (UF TRUE s2)          = F s2
    alg (RF FALSE s2)         = G s2
    -- On the rest we simplify only the sub terms
    alg x                     = embed x
