# Haskell tool to convert: LearnLib models + RERS LTL -> NuSMV

Code for converting the LTL formulae and the dot models to the NuSMV syntax.
To build, use `stack build`. To run, look for the (only) executable in the
`.stack-work` directory, I guess.
