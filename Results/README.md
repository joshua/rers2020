# Results

The directory `LTL-Models` contains the models learned by
LearnLib. They may be incomplete, but we can model check
them anyways. Surprisingly, they are relatively small
(< 100 states).

In `Reachability-Models` we find the learned models of the
reachability problems. We know for sure that problems 12,
15, and 16 are incomplete (they were still learning after
90 hours). The others we don't know, they passed 2 milion
tests.

In `Reachability-Results` we have listed the reachable
error codes from the current (possibly incomplete) models.

