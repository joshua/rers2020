% !TeX spellcheck = en_GB
\documentclass[11pt,a4paper]{article}

\usepackage[utf8]{inputenc} % utf8, also in bib
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{wrapfig} % float on the side
\usepackage[font=small]{caption} % smaller captions
\usepackage{mathpazo, tgpagella} % nice font
\usepackage{microtype} % better punctuation spacing
\usepackage{hyperref} % urls in bib
\usepackage{graphicx}

\usepackage{sectsty} % italic headings
\allsectionsfont{\mdseries\itshape} % italice headings

\usepackage{booktabs} % nicer lines for tables
\setlength{\tabcolsep}{9pt} % wider columns in tables
\newcommand*{\tbhead}[1]{\multicolumn{1}{c}{\it #1}}

\newcommand*{\inlcode}[1]{\,\texttt{#1}\,}
\newcommand{\todo}[1]{\raisebox{.25ex}{\framebox{\tiny\sc Todo}}\marginpar{{\tiny #1\par}}}

\theoremstyle{definition}
\newtheorem{remark}{Remark}

\author{Joshua Moerman \\ RWTH Aachen University \\ joshua@cs.rwth-aachen.de \and Jana Berger \\ 
RWTH Aachen University \\ berger@cs.rwth-aachen.de}
\title{RERS 2020:\\Learning, Testing, Fuzzing and Slicing}
\date{October 16, 2020}
\begin{document}
\maketitle


\section{Overview}

The RERS challenge consists of analysis problems for reactive software.
We participate in the following two tracks.
\begin{description}
	\item[Sequential LTL problems]
	These consist of source code (in both C and Java) and we have to evaluate whether certain LTL properties hold for the program.
	\item[Sequential reachability problems]
	These consist of source code (again both C and Java) and we have to decide which error codes are reachable.
\end{description}

Our main technique is that of \emph{active automata learning}.
For the LTL problems, we hope that the learning finds the right model, in which case we can \emph{model-check} the LTL properties, since this logic is decidable for finite state systems.
For the reachability problems, we can only find reachable errors through learning.
For non-reachability, we have to rely on the fact that the learned models are complete.
We have observed that our models were not complete, and so we used another technique: \emph{program slicing}.
This was able to prove some non-reachability results.
For both tracks we did additional \emph{testing} and \emph{fuzzing}.


\subsection{Tools}

Here we give a list of used tools with a short description.

\begin{description}
	\item[LearnLib]~\cite{IsbernerHS15} (All problems) \hfill \\
	This is a Java library for automata learning and testing.
	We have used this to learn all the provided problems.

	\item[NuSMV]~\cite{CimattiCGR99} (LTL problems) \hfill \\
	This is an explicit state model checker, which we use after the models have been learned.

	\item[hybrid-ads]~\cite{Moerman19} (LTL problems) \hfill \\
	We did additional testing with our own tool, which is based on distinguishing sequences.
	
	\item[afl-fuzz]~\cite{ZalewskiAFL} (Reachability problems) \hfill \\
	We used fuzzing to do additional testing of the reachability problems.
	This is closely related to random testing, but it also instruments the code, in order to discover different execution paths.
	
	\item[FramaC]~\cite{DBLP:conf/sefm/CuoqKKPSY12} (Reachability problems) \hfill \\
	This is a library for many types of analysis for C code.
	We used slicing to prove that certain errors are unreachable.
\end{description}

All commands, used parameters, and results can be found in our repository:
\href{https://git.rwth-aachen.de/joshua/rers2020}{git.rwth-aachen.de/joshua/rers2020}


\subsection{Timeline}

Since learning, testing, and fuzzing may run indefinitely, we have set fixed time outs.
We did the following.
The actual running times can be found in the subsequent sections.

\begin{itemize}
	\item 24 hours of learning, with max 2 000 000 tests (per hypothesis).
	\item 90 hours of learning for known incomplete problems (12, 15, 16).
	\item 112 hours of additional fuzzing.
	\item 112 hours of additional testing.
\end{itemize}

After that, we did the model checking for the LTL problems.
And we extracted the reachable errors for the reachability problems.
Additionally we did slicing for the reachability problems.
All these later steps did not take a considerable amount of time.

\begin{remark}
For slicing, we had time issues for the biggest problem (Problem 19).
It needed roughly a full day \emph{per error code} to slice the problem.
For this reason, we did not slice Problem 19.
\end{remark}


\section{Learning}

In this section we report more details on the learning.
We have used the TTT algorithm~\cite{IsbernerHS14} for learning and the randomised Wp method~\cite{FujiwaraBKAG91, Moerman19} for testing the hypotheses.
Otherwise, the LearnLib code was straightforward.
The number of tests was bound by 2 million per hypothesis.

\begin{table}
	\centering\small
	\begin{tabular}{c r r r l}
		\tbhead{Problem} & \tbhead{States} & \tbhead{Time} &  \tbhead{EQs} & \tbhead{Remark} \\ \midrule
		1    &   35    &  5s       & 15 & \\
		2    &   55    &  8s       & 30 & \\
		3    &   107   &  5m 44s   & 70 & \\
		4    &   88    &  31s      & 59 & \\
		5    &   151   &  23s      & 72 & \\
		6    &   96    &  1m 4s    & 51 & \\
		7    &   107   &  10m 29s  & 60 & \\
		8    &   42    &  1m 59s   & 23 & \\
		9    &   77    &  20m 19s  & 35 & \\
		\midrule
		11   &   20    &  1s         & 7 & \\
		12   &  9 724  &  90h 0m 0s  & 3770 & Timeout \\
		13   &   77    &  40s        & 41 & \\
		14   &   162   &  27s        & 77 & \\
		15   &  11 238 &  90h 0m 0s  & 6152 & Timeout \\
		16   &  13 895 &  90h 0m 0s  & 7914 & Timeout \\
		17   &   714   &  3m 0s      & 372 & \\
		18   &   658   &  2m 53s     & 339 & \\
		19   &   884   &  32m 23s    & 485 & \\
	\end{tabular}
	\caption{Runtime to the final hypothesis and number of equivalence queries.
		Note this time does not include testing \emph{after} the last hypothesis.
		Some of the reachability problems timed out.}
	\label{tab:learning-times}
\end{table}

The running times for learning can be found in Table~\ref{tab:learning-times}.
Note that some problems ran until the timeout, meaning that these models are known to be incomplete.
(For the other models, we do not know whether they are complete.)
Unfortunately, we did not count the number of membership queries.

\begin{remark}
	We observe that the problems 17 and 18 are quite big, but are learned very fast.
	This is in contrast to, for example, problem 9.
	We conjecture that the running time is mostly determined by the time to find counterexamples (based on luck) and that the learning algorithm is efficient.
\end{remark}


\section{Testing and Fuzzing}

After the learning process we did additional testing.
For the LTL problems, we did this with our FSM-based conformance checking \emph{hybrid-ads}~\cite{Moerman19}.
This tool randomly generates tests according to the HSI-method (using adaptive distinguishing sequences when possible).
We tested for 112 hours, slowly increasing the length of the length of the tests.
\emph{No counterexamples were found.}
To give an idea of the number of tests, for problem 1:
We performed roughly 67 million tests, of an average length of 1030 actions.
Meaning that we roughly performed 69 billion tests, which, admittedly, is a bit overkill.

\begin{table}
	\centering\small
	\begin{tabular}{c c c c}
		\tbhead{Problem} & \tbhead{Common findings} & \tbhead{Unique to learning} & \tbhead{Unique to fuzzing} \\ \midrule
		11   &   18   &     &    \\
		12   &   16   &     &    \\
		13   &   23   &     &  8 \\ \toprule[0.1pt]
		14   &   15   &     &    \\
		15   &   41   &     &    \\
		16   &   14   &     &  1 \\ \toprule[0.1pt]
		17   &   30   &     &    \\
		18   &   29   &  1  &    \\
		19   &   13   &  1  &    \\
	\end{tabular}
	\caption{Number of reachable errors found by the different techniques.}
	\label{tab:fuzzing}
\end{table}

For the reachability problems, we used fuzzing instead.
The reason for this is that we expected counterexamples to be found (because problems 12, 15, and 16 timed out).
We wanted to only look for reachable errors, not just any counterexample, and fuzzing finds traces for which the program crashes.
We did provide the fuzzer an initial state cover, so that it is already able to find the known errors quickly.
The fuzzer found additional error codes, which we summarise in Table~\ref{tab:fuzzing}.
Note that for the known incomplete models it only found 1 additional error.
But it also found many more errors for problem 13.

\begin{remark}
	The fuzzing did find additional errors to be reachable.
	However, it also didn't find all the errors found through learning.
	This is surprising, as we provided the fuzzer a state cover of the learned models.
	The fuzzer outputted that there were too many initial traces, and that it removed redundant ones.
	This means that, although our state cover reaches different states, these are seen as equivalent by the fuzzer.
\end{remark}


\section{LTL Model Checking}

Since the learning algorithm constructs finite state machines, we can directly apply LTL model checking.
We used the NuSMV tool, for no particular reason.
The above mentioned repository contains code for converting LearnLib models into NuSMV models, by using the alternating I/O semantics.

\begin{figure}
	\centering
	\newcommand{\ltltimelinescale}{0.32}
	\includegraphics[scale=\ltltimelinescale]{images/table1-margins}
	\includegraphics[scale=\ltltimelinescale]{images/table2-margins}
	\includegraphics[scale=\ltltimelinescale]{images/table3-margins}
	\includegraphics[scale=\ltltimelinescale]{images/table4-margins}
	\includegraphics[scale=\ltltimelinescale]{images/table5-margins}
	\includegraphics[scale=\ltltimelinescale]{images/table6-margins}
	\includegraphics[scale=\ltltimelinescale]{images/table7-margins}
	\includegraphics[scale=\ltltimelinescale]{images/table8-margins}
	\includegraphics[scale=\ltltimelinescale]{images/table9-margins}
	\caption{Evaluating all the LTL properties on the intermediate hypotheses.
		The problems are in order (first left-to-right, then top-to-bottom).}
	\label{fig:ltl-timelines}
\end{figure}

It is interesting to see how the valuation of the LTL properties change over time.
We have computed all the properties on the intermediate hypotheses as well.
We observe that the valuation doesn't change very often, and that the ``final'' result is obtained before the final hypothesis.
The first hypothesis is always a single-state machine.
See Figure~\ref{fig:ltl-timelines} for these timelines.

We note that some properties don't change at all, they probably are tautologies, or have little to do with the actual program.
Some properties only change once, these could be safety properties, as they can be violated with a single finite word.
We do not know whether there is any connection between the ease of learning and the complexity of the property.


\section{Slicing}

We select a specific error code to be sliced and modify the program to only contain the error locations of this error code.
FramaC then removes all the code which it deems irrelevant for that particular error.
We achieve this by slicing for calls to \texttt{\_\_VERIFIER\_error()}.
In some cases, no code was left (i.e. just an empty \inlcode{main()} function), meaning that those error codes are unreachable.
In Table~\ref{tab:slicing} we report the number of unreachable error codes proven this way.

\begin{table}
	\centering\small
	\begin{tabular}{c c c c c}
		\tbhead{Problem} & \tbhead{Reachable} & \tbhead{Unreachable} & \tbhead{Unknown}  & \tbhead{Avg. Time per Slice} \\ \midrule
		11   &   18   &  64   & 18   &  2.15s  \\
		12   &   16   &  49   & 35   &  7.82s   \\
		13   &   31   &  22   & 47   &  9.93s   \\ \toprule[0.1pt]
		14   &   15   &  43   & 42   &  9.02s   \\
		15   &   41   &  16   & 43   &  248s   \\
		16   &   15   &  2    & 83   &  1488s   \\ \toprule[0.1pt]
		17   &   30   &  26   & 44   &  54.6s   \\
		18   &   30   &  18   & 52   &  1954s   \\
		19   &   14   &  0    & 86   &  $>$86400s   \\
	\end{tabular}
	\caption{Number of reachable errors (see previous section) and unreachable errors as proven by program slicing.
		We have added a column which indicates for how many error codes we do not have an answer.}
	\label{tab:slicing}
\end{table}

Unfortunately, this approach is really separate from the learning, and we see no way of using the slicing information for the learning, or vice versa.


\small
\bibliographystyle{alpha}
\bibliography{references}


\end{document}