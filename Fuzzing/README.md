Fuzzing
=======

Used to, after the learning is finished, validate our findings.
We already know that the problems 12, 15, 16 are incomplete,
because they were not finished learning within the time limit.

Fuzzing just executes a whole lot of inputs to the binaries and
see which inputs crash. These crashes correspond to reachable
errors, and the traces can be re-run.

## Result:
For the problems 13, 16, the fuzzer found more reachable errors.
That means, that problem 13 was also not finished with learning.

We fuzzed for 112 hours. And we initialised the test sequences
with a state cover from the learning process.


## How to generate initial test suite

I have used my testing tool (hybrid-ads, see Testing), to generate
a state-cover for the models. These sequences guide the fuzzer to
interesting places (hopefully). For the small ones, I also added
suffixes. But for the big ones, that would be too much.

Note: afl-fuzzer told me that there are too many test cases.

Command for problems i = 11 13 14 17 18 19
```
gunzip -c ../../rers2020/Results/Reachability-Models/Problem$i.dot.gz | ./main -k 0 -m fixed > ../../rers2020/Fuzzing/testsuite$i.txt
```

Command for problems i = 12 15 16
```
gunzip -c ../../rers2020/Results/Reachability-Models/Problem$i.dot.gz | ./main -k 0 -m fixed -s none > ../../rers2020/Fuzzing/testsuite$i.txt
```

