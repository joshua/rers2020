# rers2020

Please refer to `Docs/report.tex` for a readable report
on what is to be found here :-).


Stuff for the RERS (Rigorous Examination of Reactive Systems)
challenge 2020. Not sure yet what we will be doing. Probably:

* Automata learning

* Program slicing

* Model checking


Content:

* `LearnLib`: a java program to learn the behaviour of the
  executables. It uses the LearnLib framework. There are
  several options (number of tests, depths of tests, etc)

* `Problems`: the original challenge. We will try not to
  change these files, so that we always have the actual
  problems.

* `Scripts`: some handy tools:

  * `buil_all.sh`: builds all problems (c files) with
    optimisation. Output will be in the `bin` directory.

  * `get_errors_gz.sh`: get error codes from LearnLib
    models. These are definetely reachable.

  * `slice.php`: uses Frama-C to slice a selected number
    of C programs for each of the error properties.

  * `slurm_job.sh`: To run the learning algorithm on the
    slurm cluster.

  * `verifier_error.c`: default implementation of the
    provided error hook: print error and abort. We use
    this in the learning.

* `Slicing`: contains the results of the slicing efforts.

* `README.md`: this file.

