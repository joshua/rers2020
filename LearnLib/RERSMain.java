import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import de.learnlib.acex.analyzers.AcexAnalyzers;
import de.learnlib.algorithms.ttt.mealy.TTTLearnerMealy;
import de.learnlib.api.algorithm.LearningAlgorithm.MealyLearner;
import de.learnlib.api.oracle.EquivalenceOracle;
import de.learnlib.api.oracle.MembershipOracle.MealyMembershipOracle;
import de.learnlib.api.query.DefaultQuery;
import de.learnlib.api.query.Query;
import de.learnlib.oracle.equivalence.MealyRandomWpMethodEQOracle;
import net.automatalib.serialization.dot.GraphDOT;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import net.automatalib.words.WordBuilder;
import net.automatalib.words.abstractimpl.AbstractAlphabet;

import java.io.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.zip.GZIPOutputStream;

/**
 * Simple learning set up for the RERS challenge.
 */
public class RERSMain {
    public static void main(String[] argv) {
        try {
            innerMain(argv);
        } catch (Exception e) {
            System.err.println("Exception " + Arrays.toString(argv));
            e.printStackTrace();
            System.exit(1);
        }
    }

    public static void innerMain(String[] argv) throws IOException {
        // First we parse the command line options
        CommandLineOptions opts = new CommandLineOptions();
        {
            JCommander jc = JCommander.newBuilder().addObject(opts).build();
            jc.parse(argv);

            if (opts.help) {
                jc.usage();
                return;
            }
        }

        // Create a logger, which also writes to file
        TeePrintStream logger = new TeePrintStream(System.out);
        if (!opts.outputPath.isEmpty()) {
            File dir = new File(opts.outputPath);
            dir.mkdir();
            dir = new File(opts.outputPath + "/hypotheses");
            dir.mkdir();
            logger.addOutput(opts.outputPath + "/output.log");
        }

        // For backlog, print all used options
        logger.println("Options: " + opts);

        // We should have a binary
        if (opts.path.isEmpty()) {
            logger.println("ERROR> warning: no file given");
            return;
        }

        LocalDateTime start = LocalDateTime.now();
        logger.println("LOG> start: " + start + ", alphabet = " + opts.alphabetSize);

        Alphabet<Integer> alphabet = new IAlphabet(opts.alphabetSize);
        MealyMembershipOracle<Integer, String> mOracle = new MQ(opts.path);

        // We fix a decent testing method in this experiment.
        EquivalenceOracle.MealyEquivalenceOracle<Integer, String> eqOracle = new MealyRandomWpMethodEQOracle<>(mOracle, opts.maxDepth, opts.expectedLength, opts.testingBound, 5);

        // Fix learner: TTT
        MealyLearner<Integer, String> learner = new TTTLearnerMealy<>(alphabet, mOracle, AcexAnalyzers.LINEAR_FWD);

        // Here we will perform our experiment. I did not use the Experiment class from LearnLib, as I wanted some
        // more control.
        int stage = 0;
        learner.startLearning();

        while (true) {
            stage++;

            if (!opts.outputPath.isEmpty()) {
                String dir = opts.outputPath + "/hypotheses";
                String outputFilename = dir + "/hyp" + stage + ".dot.gz";
                OutputStreamWriter output = new OutputStreamWriter(new GZIPOutputStream(new FileOutputStream(outputFilename)));
                GraphDOT.write(learner.getHypothesisModel(), alphabet, output);
                output.close();
            }

            // Print statistics
            LocalDateTime now = LocalDateTime.now();
            logger.println(stage + ": " + now + " duration so far: " + Duration.between(start, now));
            logger.println("States: " + learner.getHypothesisModel().size());

            // Search for CE
            DefaultQuery<Integer, Word<String>> ce = eqOracle.findCounterExample(learner.getHypothesisModel(), alphabet);
            if (ce == null) break;

            // Rinse and repeat
            learner.refineHypothesis(ce);
        }

        logger.println("Done with learning " + opts.path);
    }

    // All options. JCommander automatically generates a nice help message (including the defaults) when running the program.
    public static class CommandLineOptions {
        @Parameter(description = "Path to binary", required = true)
        public String path = "";

        @Parameter(names = "-depth", description = "The depth used for fixed test methods (WMethod, LeeYannakakisFixes, ...). Used as minimum for randomized methods")
        public int maxDepth = 2;

        @Parameter(names = "-expectedLength", description = "The expected length of the middle part in randomized testing")
        public int expectedLength = 8;

        @Parameter(names = "-alphabetSize", description = "Size of alphabet", required = true)
        public int alphabetSize = 20;

        @Parameter(names = "-testingBound", description = "Maximal number of tests to run")
        public int testingBound = 10_000_000;

        @Parameter(names = "-outputPath", description = "Path for output")
        public String outputPath = "";

        @Parameter(names = {"--help", "-h"}, help = true)
        public boolean help;

        @Override
        public String toString() {
            return "CommandLineOptions{" +
                    "path='" + path + '\'' +
                    ", maxDepth=" + maxDepth +
                    ", expectedLength=" + expectedLength +
                    ", alphabetSize=" + alphabetSize +
                    ", testingBound=" + testingBound +
                    ", outputPath='" + outputPath + '\'' +
                    ", help=" + help +
                    '}';
        }
    }

    // Execute a binary and do I/O
    // Hopefully more efficiently then SULOracle
    public static class MQ implements MealyMembershipOracle<Integer, String> {
        private final ProcessBuilder pb;

        MQ(String path) {
            pb = new ProcessBuilder(path);
            pb.redirectErrorStream(true);
        }

        String queryToString(Word<Integer> prefix, Word<Integer> suffix) {
            StringBuilder ib = new StringBuilder(2 * prefix.size() + 2 * suffix.size() + 1);
            for (Integer s : prefix) {
                ib.append(s);
                ib.append(' ');
            }
            for (Integer s : suffix) {
                ib.append(s);
                ib.append(' ');
            }
            ib.append(0); // quits the program
            return ib.toString();
        }

        @Override
        public Word<String> answerQuery(Word<Integer> prefix, Word<Integer> suffix) {
            try {
                Process process = pb.start();
                Writer processInput = new OutputStreamWriter(process.getOutputStream());
                BufferedReader processOutput = new BufferedReader(new InputStreamReader(process.getInputStream()));

                // send input
                processInput.write(queryToString(prefix, suffix));
                processInput.flush();
                processInput.close();

                // wait to terminate
                process.waitFor();

                // read output
                WordBuilder<String> wb = new WordBuilder<>(suffix.size());
                int skipPrefix = prefix.length();
                while (processOutput.ready()) {
                    String line = processOutput.readLine();
                    if (skipPrefix > 0) {
                        skipPrefix--;
                        continue;
                    }
                    if (line.startsWith("_error")) {
                        if (!wb.isEmpty()) {
                            String prevLine = wb.get(wb.size() - 1);
                            wb.set(wb.size() - 1, prevLine + line);
                        }
                        break;
                    }
                    wb.add(line);
                }

                wb.repeatAppend(suffix.size() - wb.size(), (String) null);

                processOutput.close();
                process.destroy();

                return wb.toWord();
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        public void processQueries(Collection<? extends Query<Integer, Word<String>>> queries) {
            // run in parallel
            queries.parallelStream().forEach((query) -> {
                query.answer(answerQuery(query.getPrefix(), query.getSuffix()));
            });
        }
    }

    public static class IAlphabet extends AbstractAlphabet<Integer> {
        private final int min;
        private final int max;

        public IAlphabet(int bound) {
            this.min = 1;
            this.max = bound;
        }

        @Override
        public Integer getSymbol(int index) {
            return index + min;
        }

        @Override
        public int getSymbolIndex(Integer symbol) {
            return symbol - min;
        }

        @Override
        public int size() {
            return max - min + 1;
        }
    }

    // For debugging not only to System.out, but also a file
    public static class TeePrintStream extends PrintStream {
        private final List<PrintStream> otherOutputs = new ArrayList<>();

        public TeePrintStream(PrintStream orig) {
            super(orig);
        }

        public void addOutput(PrintStream os) {
            otherOutputs.add(os);
        }

        public void addOutput(String filename) throws FileNotFoundException {
            addOutput(new PrintStream(new FileOutputStream(filename)));
        }

        @Override
        public boolean checkError() {
            boolean err = super.checkError();
            for (PrintStream os : otherOutputs) {
                err = err || os.checkError();
            }
            return err;
        }

        @Override
        public void write(int x) {
            super.write(x);
            for (PrintStream os : otherOutputs) {
                os.write(x);
            }
        }

        @Override
        public void write(byte[] x, int o, int l) {
            super.write(x, o, l);
            for (PrintStream os : otherOutputs) {
                os.write(x, o, l);
            }
        }

        @Override
        public void close() {
            super.close();
            for (PrintStream os : otherOutputs) {
                os.close();
            }
        }

        @Override
        public void flush() {
            super.flush();
            for (PrintStream os : otherOutputs) {
                os.flush();
            }
        }
    }
}
