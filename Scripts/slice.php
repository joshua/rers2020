#!/usr/bin/php
<?php

$interesting = array(
	11,
	12,
	13,
	14,
	15,
	16,
	17,
	18,
	19,
);

function makePath($problemNumber, $extra = '') {
	return '../Problems/Reachability/Problem'.$problemNumber.'/Problem'.$problemNumber.$extra.'.c';
}

function sliceProgram($from, $to, $override) {
	if (!file_exists($to) || $override) {
		$output = array();
		$returnVar = -1;
		exec('frama-c -slice-calls "__VERIFIER_error" '.$from.' -then-on \'Slicing export\' -print -ocode "'.$to.'"', $output, $returnVar);
	
		if ($returnVar != 0) {
			throw new Exception("Failed to slice program '".$from."': ".implode("\r\n", $output));
		}
	}
}

function buildSingleErrorFilesFromProblem($problem, $override) {
	echo "Working on Problem '".$problem."'...\r\n";
	$listOfSingleErrFiles = array();
	
	$problemFileName = makePath($problem);
	$file = file_get_contents($problemFileName);
	if ($file === false) {
		echo "Failed to load problem!\r\n";
		return $listOfSingleErrFiles;
	}
	
	// For reference, we slice the entire program to get the "maximum" size after Frama-Cs reordering:
	$slicedFullProgramFileName = makePath($problem, '_sliced');
	echo "Slicing full program...";
	$timeStart = microtime(true);
	sliceProgram($problemFileName, $slicedFullProgramFileName, $override);
	$timeEnd = microtime(true);
	echo " Done. (took ".($timeEnd - $timeStart)."s)\r\n";
	
	// void errorCheck\(\)\s*{\s*((?:if\s*\((?:[^()]|\((?:[^()]|\((?:[^()]|\((?:[^()]|\((?:[^()]|\((?:[^()])+\))+\))+\))+\))+\))+\)\s*{\s*cf = 0;\s*__VERIFIER_error\((\d+)\);\s*}\s*)+)}
	// void errorCheck\(\)\s*{((?:[^\{\}]+(?:\{[^{}]+\}))+[^\{\}]*)}
	if (preg_match('/void errorCheck\(\)\s*{((?:[^\{\}]+(?:\{[^{}]+\}))+[^\{\}]*)}/si', $file, $result)) {
		# Successful match
		$errorCheckContents = $result[1];
		
		$minErrorNumber = PHP_INT_MAX;
		$maxErrorNumber = -1;
		$matchTexts = array();
		
		// if\s*\((?:[^()]|\((?:[^()]|\((?:[^()]|\((?:[^()]|\((?:[^()]|\((?:[^()]|\((?:[^()]|\((?:[^()])+\))+\))+\))+\))+\))+\))+\))+\)\s*{\s*cf = 0;\s*__VERIFIER_error\((\d+)\);\s*}
		preg_match_all('/if\s*\((?:[^()]|\((?:[^()]|\((?:[^()]|\((?:[^()]|\((?:[^()]|\((?:[^()]|\((?:[^()]|\((?:[^()])+\))+\))+\))+\))+\))+\))+\))+\)\s*{\s*cf = 0;\s*__VERIFIER_error\((\d+)\);\s*}/si', $errorCheckContents, $result, PREG_SET_ORDER);
		
		if (count($result) != 100) {
			echo "Warning: Found only ".count($result)." error definitions on problem ".$problem."!\r\n";
		}
		
		for ($matchi = 0; $matchi < count($result); $matchi++) {
			# Matched text = $result[$matchi][$backrefi];
			$fullMatch = $result[$matchi][0];
			$errorNumber = $result[$matchi][1];
			
			if (array_key_exists($errorNumber, $matchTexts)) {
				echo "Warning: Key ".$errorNumber." already exists for problem ".$problem."!\r\n";
			}
			
			$minErrorNumber = min($minErrorNumber, $errorNumber);
			$maxErrorNumber = max($maxErrorNumber, $errorNumber);
			$matchTexts[$errorNumber] = $fullMatch;
			
			//echo "Found error #".$errorNumber.".\r\n";
		}
		
		$total = $maxErrorNumber - $minErrorNumber + 1;
		echo "Error Numbers go from ".$minErrorNumber." to ".$maxErrorNumber.", total = ".$total.".\r\n";
		
		$timeStart = microtime(true);
		// Produce one file per error number, containing only that specific error number statement.
		for ($i = $minErrorNumber; $i <= $maxErrorNumber; ++$i) {
			$singleErrNumFile = str_replace($errorCheckContents, $matchTexts[$i], $file);
			$singleErrNumFileName = makePath($problem, '_only_'.sprintf('%04d', $i));
			file_put_contents($singleErrNumFileName, $singleErrNumFile);
			$listOfSingleErrFiles[$i] = $singleErrNumFileName;
			
			// Debug: Only one.
			//break;
		}
		$timeEnd = microtime(true);
		echo "Producing single-error files took ".($timeEnd - $timeStart)."s.\r\n";
	} else {
		# Match attempt failed
		echo "Failed to match errorCheck function on problem ".$problem."!\r\n";
		file_put_contents($problem."_errorCheckContents.base64", base64_encode($file));
	}
	return $listOfSingleErrFiles;
}

function sliceSingleErrPrograms($problem, $listOfSingleErrFiles, $override) {
	$listOfSlicedPrograms = array();
	
	if (count($listOfSingleErrFiles) > 0) {
		$timeStart = microtime(true);
		foreach ($listOfSingleErrFiles as $i => $singleErrNumFileName) {
			echo "Slicing file #".$i."...\r\n";
			$slicedSingleErrNumFileName = makePath($problem, '_sliced_'.sprintf('%04d', $i));
			sliceProgram($singleErrNumFileName, $slicedSingleErrNumFileName, $override);
			$listOfSlicedPrograms[$i] = $slicedSingleErrNumFileName;
		}
		$timeEnd = microtime(true);
		echo "Slicing the files took ".($timeEnd - $timeStart)."s (".(($timeEnd - $timeStart) / count($listOfSingleErrFiles))."s avg per file).\r\n";
	}
	
	return $listOfSlicedPrograms;
}

$override = FALSE;

foreach ($interesting as $problem) {
	$listOfSingleErrFiles = buildSingleErrorFilesFromProblem($problem, $override);

	$listOfSlicedPrograms = sliceSingleErrPrograms($problem, $listOfSingleErrFiles, $override);
	
	$listOfSafePrograms = array();
	foreach ($listOfSlicedPrograms as $i => $slicedProgram) {
		$file = file_get_contents($slicedProgram);
		if (stripos($file, '__VERIFIER_error') === FALSE) {
			$listOfSafePrograms[] = $i;
		}
	}
	
	echo "For problem ".$problem.", error codes ".implode(', ', $listOfSafePrograms)." have been deemed safe by static analysis.\r\n";
	file_put_contents('../Results/Reachability-Results/Problem'.$problem.'.unreachable', implode("\n", $listOfSafePrograms)."\n");
}

echo "Done.\r\n";
