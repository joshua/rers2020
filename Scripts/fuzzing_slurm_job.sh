#!/usr/bin/env zsh

### The jobs can be scheduled with 'sbatch slurm_job.sh'
### It has subtasks

#SBATCH -A moves
#SBATCH --job-name=rers2020_fuzzing
#SBATCH --output=/home/tl416220/rers2020/output/%x.job%A_task%4a.out
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=4G
#SBATCH -t 112:00:00

### Number of tasks
#SBATCH --array=11-19


### Actual script starts here

setopt err_exit       # exit on error
setopt nounset        # error on undefined variables
setopt xtrace         # show commands

# Get our task number
cur=$SLURM_ARRAY_TASK_ID

# Get executables
rershome=/home/tl416220/rers2020
afl=/home/tl416220/tools/afl-2.52b/afl-fuzz
problem=/work/tl416220/rers2020-bin-fuzzing/Problem$cur

mkdir -p /work/tl416220/rers2020-fuzzing/Problem$cur
cd /work/tl416220/rers2020-fuzzing/Problem$cur

mkdir -p findings
mkdir -p testcases

# split initial test suite into separate files
split -l 1 -a 4 $rershome/Fuzzing/testsuite$cur.txt testcases/test_

# Run afl
AFL_SKIP_CRASHES=1 AFL_SKIP_CPUFREQ=1 AFL_NO_AFFINITY=1 $afl -i testcases -o findings $problem

# On my machine I had to change the core dump handler:
# Luckily, we do not need that on the cluster
#
# root@coltrane:/proc/sys/kernel# cat core_pattern 
# |/usr/share/apport/apport %p %s %c %d %P %E
# root@coltrane:/proc/sys/kernel# echo core > core_pattern 

# To get error codes from findings:
# for f in findings/crashes/*; do cat $f | ../bin/problems-afl/Problem12 2>/dev/null | grep error; done | sort -u

