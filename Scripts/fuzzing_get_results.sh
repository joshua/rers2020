#!/bin/zsh

# Gets error codes from all fuzzing stuff.

setopt err_exit       # exit on error
setopt nounset        # error on undefined variables

for i in $(seq 11 19); do
    fuzzpath="/work/tl416220/rers2020-fuzzing/Problem$i"
    binpath="/work/tl416220/rers2020-bin-fuzzing/Problem$i"
    output="/home/tl416220/rers2020/Fuzzing/reachable_errors_$i.txt"

    # Run all the crashes, and observe output
    # Then grep the errors, extract the numbers and sort them.
    for f in $fuzzpath/findings/crashes/*; do cat $f | $binpath 2>/dev/null | grep "error"; done | sed 's/.*error_\([0-9]*\).*/\1/' | sort -un > $output
done
