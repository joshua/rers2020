#!/bin/zsh

# Converts the learned models into the nusmv format, together with
# the LTL formulas. It saves the smv files somewhere. You should
# first compile the haskell tool. See the NuSMV directory.

setopt err_exit       # exit on error
setopt nounset        # error on undefined variables
setopt xtrace         # show commands

converter="./NuSMV/.stack-work/dist/x86_64-linux-tinfo6/Cabal-3.0.1.0/build/convert/convert"

inputmodelsdir="./Results/LTL-Models"
inputltldir="./Problems/LTL"
outputsmvdir="./Results/LTL-NuSMV"

for i in $(seq 1 9); do
  inputmodel="$inputmodelsdir/Problem$i.dot.gz"
  inputltl="$inputltldir/Problem$i/constraints-Problem$i.txt"
  outputsmv="$outputsmvdir/Problem$i.smv"

  $converter <(gunzip -c $inputmodel) $inputltl > $outputsmv
done
