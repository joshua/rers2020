#!/bin/zsh

# Computes all properties on all intermediate hypotheses

setopt err_exit       # exit on error
setopt nounset        # error on undefined variables
setopt xtrace         # show commands

converter="./NuSMV/.stack-work/dist/x86_64-linux-tinfo6/Cabal-3.0.1.0/build/convert/convert"
nusmv="/home/joshua/Documents/Projects/NuSMV-2.6.0-Linux/bin/NuSMV"

for i in $(seq 1 9); do
  inputmodelsdir="./local/Problem$i/hypotheses"
  inputltldir="./Problems/LTL/Problem$i"
  outputdir="./local/tables"

  for hypo in $(ls $inputmodelsdir | sort -V); do
    inputmodel="$inputmodelsdir/$hypo"
    inputltl="$inputltldir/constraints-Problem$i.txt"
    outputtable="$outputdir/Problem$i.$hypo.txt"

    # One of the best one-liners I have written
    $nusmv -keep_single_value_vars -dcx <($converter <(gunzip -c $inputmodel) $inputltl) | grep "specification" | sed 's/.* //' > "$outputtable"
  done

  # combine into a single table
  paste -d, $(ls $outputdir/Problem$i.* | sort -V) | sed 's/false/✗/g;s/true/✓/g' > "$outputdir/table$i.csv"
done
