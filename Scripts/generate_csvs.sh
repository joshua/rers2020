#!/bin/zsh

# Compiles all results into csv files

setopt err_exit       # exit on error
setopt nounset        # error on undefined variables
setopt shwordsplit    # old splitting behaviour
setopt xtrace         # show commands

dir="./Results/Submission"

mkdir -p $dir

# The LTL problems
for i in $(seq 1 9); do
    infile="./Results/LTL-NuSMV/Problem$i.out"
    outfile="$dir/problem$i.csv"

    # the -n flag adds line numbers
    cat -n $infile | awk -v prob="$i" '{print prob "," $1 "," $2}' > $outfile
done

# The reachability problems
for i in $(seq 11 19); do
    infile1="./Results/Reachability-Results/Problem$i.reachable"
    infile2="./Results/Reachability-Results/Problem$i.unreachable"
    outfile="$dir/problem$i.csv"
    cat $infile1 | awk -v prob="$i" '{print prob "," $1 ",true" }' > "$dir/problem$i.csv"

    # If we have computed unreachable errors, also include them
    if [[ -f $infile2 ]];then
        cat $infile2 | awk -v prob="$i" '{print prob "," $1 ",false" }' >> $outfile

        # Sort for convenience
        sort -n -o $outfile $outfile
    fi
done
