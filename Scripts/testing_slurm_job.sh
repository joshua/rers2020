#!/usr/bin/env zsh

### The jobs can be scheduled with 'sbatch slurm_job.sh'
### It has subtasks

#SBATCH -A moves
#SBATCH --job-name=rers2020_testing
#SBATCH --output=/home/tl416220/rers2020/output/testing_%x.job%A_task%4a.out
#SBATCH --cpus-per-task=2
#SBATCH --mem-per-cpu=4G
#SBATCH -t 112:00:00

### Number of tasks
#SBATCH --array=1-9


### Actual script starts here

setopt err_exit       # exit on error
setopt nounset        # error on undefined variables

# Since our tool is built with gcc9, we needs its ABI and so on
module load DEVELOP
module load gcc/9

cur=$SLURM_ARRAY_TASK_ID

tester="/home/tl416220/tools/hybrid-ads/build/rersmain"
problemModel="/home/tl416220/rers2020/Results/LTL-Models/Problem$cur.dot.gz"
problemExec="/work/tl416220/rers2020-bin/Problem$cur"

cexFile="/work/tl416220/rers2020-testing/CexProblem$cur.txt"

# minimal length (grows linearly)
k=0

# plus expected length (grows quadratically)
r=8
j=0

maxtime=100m

while true; do
  echo "k = $k and r = $r on $(date)"

  # we kill it and try again, this re-randomises the data structures
  # and we increase the length of tests
  gunzip -c $problemModel | timeout $maxtime $tester -k $k -r $r -c $problemExec | tee -a $cexFile
  sleep 1m

  cexSize=$(wc -c < $cexFile)

  # if we have 100 kilobyte worth of counterexamples, we might as well stop
  if [ $cexSize -ge 100000 ]; then
    break
  fi
  
  ((k = k + 2))
  ((j = j + 2))
  ((r = r + j))
done
