#!/bin/zsh

# Compiles all the c files from the Problems directory. Outputs all
# binaries in ./bin/problems. Uses the afl-gcc compiler to inject
# instrumentation.

setopt err_exit       # exit on error
setopt nounset        # error on undefined variables
setopt shwordsplit    # old splitting behaviour
setopt xtrace         # show commands

CC=/home/tl416220/tools/afl-2.52b/afl-gcc
CFLAGS=-O3
DIR=./bin/problems-afl

mkdir -p $DIR

for problem in $(find ./Problems/Reachability -name "*.c"); do
    name=$(basename -s .c $problem)
    # The scanf call in the problems doesn't work well.
    # It should gracefully exit when it cannot parse a number.
    # So we patch this temporarily.
    tmpfile=$(mktemp $name.fixed.XXX.c)
    sed 's/\(scanf("%d", &input)\)/if(\1 != 1) return -1/' "$problem" > "$tmpfile"
    $CC $CFLAGS "./Scripts/fuzzing_verifier_error.c" "$tmpfile" -o "$DIR/$name"
    rm $tmpfile
done

if [ $(hostname) = "login18-1.hpc.itc.rwth-aachen.de" ]; then
  DIR2=/work/tl416220/rers2020-bin-fuzzing
  rm -f $DIR2
  cp -r $DIR /work/tl416220/rers2020-bin-fuzzing
fi

