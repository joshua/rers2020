#!/bin/zsh

# Compiles all the c files from the Problems directory. Outputs all
# binaries in ./bin/problems. Uses whatever compiler is at `cc`.
# See http://www.rers-challenge.org/2020/index.php?page=c-code .
# Expects to be executed from the `rers2020` directory.

setopt err_exit       # exit on error
setopt nounset        # error on undefined variables
setopt shwordsplit    # old splitting behaviour
setopt xtrace         # show commands

CC=cc
CFLAGS=-O3
DIR=./bin/problems

mkdir -p $DIR

for problem in $(find ./Problems -name "*.c"); do
    name=$(basename -s .c $problem)
    $CC $CFLAGS "./Scripts/verifier_error.c" "$problem" -o "$DIR/$name"
done
