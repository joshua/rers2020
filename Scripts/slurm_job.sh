#!/usr/bin/env zsh

### The jobs can be scheduled with 'sbatch slurm_job.sh'
### It has subtasks

#SBATCH -A moves
#SBATCH --job-name=rers2020
#SBATCH --output=/home/tl416220/rers2020/output/%x.job%A_task%4a.out
#SBATCH --cpus-per-task=3
#SBATCH --mem-per-cpu=8G
#SBATCH -t 12:00:00

### Number of tasks
#SBATCH --array=1-18


### Actual script starts here

# I have *copied* stuff from my home to /work
# Don't forget to copy again, if changed
cd /work/tl416220/rers2020-learnlib

# Get our task number, and extract n-th line from the file
cur=$SLURM_ARRAY_TASK_ID
cmd=$(time sed -n "${cur}p" < run_jobs.txt)
$cmd

