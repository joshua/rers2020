#!/bin/zsh

# Gets error codes from the learned models.
# Provide filename (gz file from learnlib) as argument.

setopt err_exit       # exit on error
setopt nounset        # error on undefined variables

gunzip -c $1 | grep "error" | sed 's/.*error_\([0-9]*\).*/\1/' | sort -un

