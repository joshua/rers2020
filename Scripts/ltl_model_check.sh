#!/bin/zsh

# Runs NuSMV on all the models, and extracts the truth.

setopt err_exit       # exit on error
setopt nounset        # error on undefined variables
setopt xtrace         # show commands

nusmv="/home/joshua/Documents/Projects/NuSMV-2.6.0-Linux/bin/NuSMV"

inputdir="./Results/LTL-NuSMV"
outputdir="./Results/LTL-NuSMV"

for i in $(seq 1 9); do
  # greps the line with the results, and takes the last word (= true or false)
  $nusmv -dcx "$inputdir/Problem$i.smv" | grep "specification" | sed 's/.* //' > "$outputdir/Problem$i.out"
done
