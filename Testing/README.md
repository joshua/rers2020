Testing
=======

I did some more testing with my testing tool at
https://git.rwth-aachen.de/joshua/hybrid-ads

For the cluster we need a newer gcc version. This
can be enabled with modules:

```
module load DEVELOP
module load gcc/9
```

